package com.golonko.catering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlainCateringApplicationTests {

    private static final Logger LOG = LoggerFactory.getLogger(PlainCateringApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PlainCateringApplication.class, args);
    }
}
