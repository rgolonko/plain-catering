package com.golonko.catering.order.controller;

import com.golonko.catering.PlainCateringApplicationTests;
import com.golonko.catering.config.SecurityConfig;
import com.golonko.catering.config.security.JwtRequestFilter;
import com.golonko.catering.order.controller.OrderControllerTest.OrderControllerConfig;
import com.golonko.catering.order.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = OrderController.class,
            excludeAutoConfiguration = SecurityAutoConfiguration.class,
            excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {SecurityConfig.class, JwtRequestFilter.class})
)
@Import(OrderControllerConfig.class)
@ContextConfiguration(classes = PlainCateringApplicationTests.class)
public class OrderControllerTest {

    private static final Logger LOG = LoggerFactory.getLogger(OrderControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ApplicationContext applicationContext;

    @MockBean
    private OrderService orderService;

    @Test(expected = NoSuchBeanDefinitionException.class)
    public void failWhenLoadExcludedBeanWebSecurityConfig() throws Exception {
        final SecurityConfig webSecurityConfig = applicationContext.getBean(SecurityConfig.class);
    }

    @Test(expected = NoSuchBeanDefinitionException.class)
    public void failWhenLoadExcludedJwtRequestFilter() throws Exception {
        final JwtRequestFilter jwtRequestFilter = applicationContext.getBean(JwtRequestFilter.class);
    }

    @TestConfiguration
    static class OrderControllerConfig {
    }
}
