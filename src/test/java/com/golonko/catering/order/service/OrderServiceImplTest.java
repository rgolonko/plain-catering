package com.golonko.catering.order.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.golonko.catering.PlainCateringApplicationTests;
import com.golonko.catering.base.AddressDto;
import com.golonko.catering.base.AddressMapper;
import com.golonko.catering.base.AddressOrderDto;
import com.golonko.catering.base.entity.Address;
import com.golonko.catering.config.security.AuthenticationFacade;
import com.golonko.catering.diet.dto.DietDto;
import com.golonko.catering.diet.dto.DietMapper;
import com.golonko.catering.diet.entity.Diet;
import com.golonko.catering.diet.repository.DietRepository;
import com.golonko.catering.order.dto.CreateOrderDto;
import com.golonko.catering.order.dto.CreateOrderMapper;
import com.golonko.catering.order.dto.OrderDietDto;
import com.golonko.catering.order.dto.OrderDietMapper;
import com.golonko.catering.order.dto.OrderDto;
import com.golonko.catering.order.dto.OrderMapper;
import com.golonko.catering.order.entity.DeliveryTime;
import com.golonko.catering.order.entity.Order;
import com.golonko.catering.order.entity.OrderDiet;
import com.golonko.catering.order.entity.PaymentStatus;
import com.golonko.catering.order.entity.QOrder;
import com.golonko.catering.order.repository.OrderDietRepository;
import com.golonko.catering.order.repository.OrderRepository;
import com.golonko.catering.user.entity.Role;
import com.golonko.catering.user.entity.User;
import com.golonko.catering.user.repository.UserRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(AuthenticationFacade.class)
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = PlainCateringApplicationTests.class)
public class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private DietRepository dietRepository;

    @Mock
    private OrderDietRepository orderDietRepository;

    @Mock
    private User user;

    @Captor
    private ArgumentCaptor<Order> argument;

    private CreateOrderMapper createOrderMapper = new CreateOrderMapper(new AddressMapper());

    private OrderMapper orderMapper = new OrderMapper(new AddressMapper());

    private OrderDietMapper orderDietMapper = new OrderDietMapper(new DietMapper());

    private OrderService orderService;

    @Before
    public void setUp() {
        orderService = new OrderServiceImpl(orderRepository, userRepository, dietRepository,
                                            createOrderMapper, orderMapper, orderDietMapper, orderDietRepository
        );
        mockStatic(AuthenticationFacade.class);
        when(AuthenticationFacade.getIdLoggedInUser()).thenReturn(1L);
        when(userRepository.findByIdOrThrow(anyLong())).thenReturn(user);

        List<Diet> diets = createDefaultDietList();
        when(dietRepository.findByIdOrThrow(1L)).thenReturn(diets.get(0));
        when(dietRepository.findByIdOrThrow(2L)).thenReturn(diets.get(1));
        when(dietRepository.findByIdOrThrow(3L)).thenReturn(diets.get(2));
    }

    @Test
    public void addressOrderEqualsUserAddressIfTheSameAddressWasChosen() {
        Address userAddress = new Address("UserCity", "UserStreet", 100, 100);
        when(user.getAddress()).thenReturn(userAddress);

        CreateOrderDto createOrderDto = createDefaultOrderDto();
        createOrderDto.getAddressOrderDto().setTheSameAsUsersAddress(true);
        orderService.create(createOrderDto);

        verify(user, times(1)).getAddress();
        verify(orderRepository).save(argument.capture());
        assertThat(argument.getValue().getAddress()).isEqualToComparingFieldByField(userAddress);
    }

    @Test
    public void addressOrderNotEqualUserAddressIfTheSameAddressWasNotChosen() {
        CreateOrderDto createOrderDto = createDefaultOrderDto();
        orderService.create(createOrderDto);

        verify(user, never()).getAddress();
        verify(orderRepository).save(argument.capture());
        assertThat(argument.getValue().getAddress()).isEqualToComparingOnlyGivenFields(
            createOrderDto.getAddressOrderDto(),
            Address.FIELD_CITY, Address.FIELD_STREET, Address.FIELD_HOUSE_NUMBER, Address.FIELD_FLAT_NUMBER
        );
    }

    @Test
    public void orderDayEndsInWeekendIfWeekendIncluded() {
        final LocalDate expectedEndDate = LocalDate.of(2020, 4, 19);
        final CreateOrderDto createOrderDto = createDefaultOrderDto();
        createOrderDto.setStartDate(LocalDate.of(2020, 4, 15));
        createOrderDto.setOrderDays(5);
        createOrderDto.setWeekendIncluded(true);
        orderService.create(createOrderDto);

        verify(orderRepository).save(argument.capture());
        LocalDate actualEndDate = Collections.max(argument.getValue().getOrderDate());
        assertThat(actualEndDate).isEqualTo(expectedEndDate);
    }

    @Test
    public void orderDayEndsInBusinessDayIfWeekendNotIncluded() {
        final LocalDate expectedEndDate = LocalDate.of(2020, 4, 21);
        final CreateOrderDto createOrderDto = createDefaultOrderDto();
        createOrderDto.setStartDate(LocalDate.of(2020, 4, 15));
        createOrderDto.setOrderDays(5);
        createOrderDto.setWeekendIncluded(false);
        orderService.create(createOrderDto);

        verify(orderRepository).save(argument.capture());
        LocalDate actualEndDate = Collections.max(argument.getValue().getOrderDate());
        assertThat(actualEndDate).isEqualTo(expectedEndDate);
    }

    @Test
    public void orderDayEndsInTheSameWeek() {
        final LocalDate expectedEndDate = LocalDate.of(2020, 4, 16);
        final CreateOrderDto createOrderDto = createDefaultOrderDto();
        createOrderDto.setStartDate(LocalDate.of(2020, 4, 15));
        createOrderDto.setOrderDays(2);
        createOrderDto.setWeekendIncluded(true);
        orderService.create(createOrderDto);

        verify(orderRepository).save(argument.capture());
        LocalDate actualEndDate = Collections.max(argument.getValue().getOrderDate());
        assertThat(actualEndDate).isEqualTo(expectedEndDate);
    }

    @Test
    public void orderDayEndsInNextWeekIfWeekendIncluded() {
        final LocalDate expectedEndDate = LocalDate.of(2020, 4, 21);
        final CreateOrderDto createOrderDto = createDefaultOrderDto();
        createOrderDto.setStartDate(LocalDate.of(2020, 4, 15));
        createOrderDto.setOrderDays(7);
        createOrderDto.setWeekendIncluded(true);
        orderService.create(createOrderDto);

        verify(orderRepository).save(argument.capture());
        LocalDate actualEndDate = Collections.max(argument.getValue().getOrderDate());
        assertThat(actualEndDate).isEqualTo(expectedEndDate);
    }

    @Test
    public void countTotalPriceIfOneOrMoreDietsWereSet() {
        final Float expectedPriceTotal = 340F;
        orderService.create(createDefaultOrderDto());

        verify(orderRepository).save(argument.capture());
        assertThat(argument.getValue().getPriceTotal()).isEqualTo(expectedPriceTotal);
    }

    @Test
    public void loggedUserIsAssignedToTheOrder() {
        orderService.create(createDefaultOrderDto());

        verifyStatic(AuthenticationFacade.class, times(1));
        AuthenticationFacade.getIdLoggedInUser();

        verify(orderRepository).save(argument.capture());
        assertThat(argument.getValue().getUser()).isNotNull();
        assertThat(argument.getValue().getUser()).isEqualTo(user);
    }

    @Test
    public void dietsAreAssignedToTheOrder() {
        final CreateOrderDto createOrderDto = createDefaultOrderDto();
        orderService.create(createOrderDto);

        verify(orderRepository).save(argument.capture());
        assertThat(argument.getValue().getOrderDiets()).size().isEqualTo(3);
    }

    @Test
    public void returnAllOrderDietsWithDietDetailsForGivenOrder() {
        final List<OrderDiet> orderDiets = createDefaultOrderDietList();
        final List<DietDto> expectedDietDto = createDefaultDietDtoList();
        when(orderDietRepository.findAllWithDietByOrderId(anyLong())).thenReturn(orderDiets);

        List<OrderDietDto> orderDietDtos = orderService.findAllByOrderId(1L);
        assertThat(orderDietDtos).size().isEqualTo(3);
        orderDietDtos.forEach(orderDietDto -> {
            assertThat(orderDietDto.getCount()).isBetween(1, 3);
        });
    }

    @Test
    public void returnAllOrderForLoggedUserOrAdmin() {
        Predicate predicate = QOrder.order.orderDays.eq(5).and(QOrder.order.address.city.eq("TestCity"));
        Pageable pageable = PageRequest.of(0, 1, Sort.by(Sort.Direction.ASC, "startDate"));

        Predicate expectedPredicateUser = new BooleanBuilder(predicate).and(QOrder.order.user.id.eq(1L));
        Pageable expectedPageableUser = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                                                       Sort.by(Sort.Direction.DESC, Order.DEFAULT_FIELD_TO_SORT)
        );

        Page<Order> orders = new PageImpl<>(Collections.singletonList(createDefaultOrder()), pageable, 10);
        when(orderRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(orders);

        Page<OrderDto> expectedOrderDto = new PageImpl<>(Collections.singletonList(createReturnedOrderDto()), pageable, 10);

        Arrays.stream(Role.values())
              .forEach((
                           role -> {
                               when(AuthenticationFacade.getRoles()).thenReturn(Collections.singletonList(role.getFullName()));
                               if (role == Role.ADMIN) {
                                   System.out.println("Role is: " + role.name());
                                   Page<OrderDto> returnedOrderDto = orderService.findAll(predicate, pageable);
                                   verify(orderRepository).findAll(predicate, pageable);
                                   assertThat(returnedOrderDto.getNumber()).isEqualTo(0);
                                   assertThat(returnedOrderDto.getSize()).isEqualTo(1);
                                   assertThat(returnedOrderDto.getContent().containsAll(expectedOrderDto.getContent()));
                               } else {
                                   System.out.println("Role is: " + role.name());
                                   Page<OrderDto> returnedOrderDto = orderService.findAll(expectedPredicateUser, expectedPageableUser);
                                   assertThat(returnedOrderDto.getNumber()).isEqualTo(0);
                                   assertThat(returnedOrderDto.getSize()).isEqualTo(1);
                                   assertThat(returnedOrderDto.getContent().containsAll(expectedOrderDto.getContent()));
                               }
                           }
                       ));
    }

    private CreateOrderDto createDefaultOrderDto() {
        CreateOrderDto createOrderDto = new CreateOrderDto();
        AddressOrderDto addressOrderDto = new AddressOrderDto();
        List<OrderDietDto> orderDiets = createDefaultOrderDietDtoList();

        addressOrderDto.setCity("NewCity");
        addressOrderDto.setStreet("NewStreet");
        addressOrderDto.setHouseNumber(1);
        addressOrderDto.setFlatNumber(1);
        addressOrderDto.setTheSameAsUsersAddress(false);

        createOrderDto.setOrderDays(5);
        createOrderDto.setStartDate(LocalDate.of(2020, 4, 15));
        createOrderDto.setDeliveryTime(DeliveryTime.MORNING);
        createOrderDto.setAddressOrderDto(addressOrderDto);
        createOrderDto.setPhoneNumber("123-123-123");
        createOrderDto.setWeekendIncluded(false);
        createOrderDto.setOrderDiets(orderDiets);
        return createOrderDto;
    }

    private List<OrderDietDto> createDefaultOrderDietDtoList() {
        final List<OrderDietDto> orderDiets = new ArrayList<>();
        OrderDietDto orderDietDto;

        orderDietDto = new OrderDietDto();
        orderDietDto.setCount(1);
        orderDietDto.setDietId(1L);
        orderDiets.add(orderDietDto);

        orderDietDto = new OrderDietDto();
        orderDietDto.setCount(2);
        orderDietDto.setDietId(2L);
        orderDiets.add(orderDietDto);

        orderDietDto = new OrderDietDto();
        orderDietDto.setCount(3);
        orderDietDto.setDietId(3L);
        orderDiets.add(orderDietDto);

        return orderDiets;
    }

    private List<OrderDiet> createDefaultOrderDietList() {
        final Order order = mock(Order.class);
        final List<Diet> diets = createDefaultDietList();
        final List<OrderDiet> orderDiets = new ArrayList<>();
        OrderDiet firstOrderDiet = new OrderDiet(order, diets.get(0), 1);
        OrderDiet secondOrderDiet = new OrderDiet(order, diets.get(1), 2);
        OrderDiet thirdOrderDiet = new OrderDiet(order, diets.get(2), 3);
        orderDiets.add(firstOrderDiet);
        orderDiets.add(secondOrderDiet);
        orderDiets.add(thirdOrderDiet);
        return orderDiets;
    }

    private List<Diet> createDefaultDietList() {
        final List<Diet> dietList = new ArrayList<>();
        Diet firstDiet = new Diet("First", true, "First diet", 1500, 50F);
        Diet secondDiet = new Diet("Second", true, "Second diet", 2000, 55F);
        Diet thirdDiet = new Diet("Third", true, "Third diet", 2500, 60F);
        dietList.add(firstDiet);
        dietList.add(secondDiet);
        dietList.add(thirdDiet);
        return dietList;
    }

    private List<DietDto> createDefaultDietDtoList() {
        final List<DietDto> dietDtoList = new ArrayList<>();
        DietDto firstDiet = new DietDto(1L, "First", true, "First diet", 1500, 50F);
        DietDto secondDiet = new DietDto(1L, "Second", true, "Second diet", 2000, 55F);
        DietDto thirdDiet = new DietDto(1L, "Third", true, "Third diet", 2500, 60F);
        dietDtoList.add(firstDiet);
        dietDtoList.add(secondDiet);
        dietDtoList.add(thirdDiet);
        return dietDtoList;
    }

    private Order createDefaultOrder() {
        final Order order = new Order();
        final Address address = new Address("TestCity", "TestStreet", 1, 1);
        order.setId(1L);
        order.setOrderDays(5);
        order.setOrderDate(Collections.emptySet());
        order.setStartDate(LocalDate.of(2020, 4, 15));
        order.setDeliveryTime(DeliveryTime.MORNING);
        order.setComment("Test comment");
        order.setAddress(address);
        order.setPriceTotal(340F);
        order.setPhoneNumber("123-123-123");
        order.setWeekendIncluded(false);
        order.setPaymentStatus(PaymentStatus.PAID);
        order.setOrderDiets(null);
        order.setUser(null);
        return order;
    }

    private OrderDto createReturnedOrderDto() {
        final OrderDto orderDto = new OrderDto();
        final AddressDto addressDto = new AddressDto();
        addressDto.setStreet("TestStreet");
        addressDto.setCity("TestCity");
        addressDto.setHouseNumber(1);
        addressDto.setFlatNumber(1);
        orderDto.setId(1L);
        orderDto.setOrderDays(5);
        orderDto.setOrderDate(Collections.emptySet());
        orderDto.setStartDate(LocalDate.of(2020, 4, 15));
        orderDto.setDeliveryTime(DeliveryTime.MORNING);
        orderDto.setComment("Test comment");
        orderDto.setAddressDto(addressDto);
        orderDto.setPriceTotal(340F);
        orderDto.setPhoneNumber("123-123-123");
        orderDto.setWeekendIncluded(false);
        orderDto.setPaymentStatus(PaymentStatus.PAID);
        return orderDto;
    }
}

