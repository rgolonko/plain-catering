package com.golonko.catering.diet.service;

import java.util.List;
import java.util.stream.Collectors;

import com.golonko.catering.config.security.AuthenticationFacade;
import com.golonko.catering.diet.dto.DietDto;
import com.golonko.catering.diet.dto.DietMapper;
import com.golonko.catering.diet.entity.Diet;
import com.golonko.catering.diet.repository.DietRepository;
import com.golonko.catering.user.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CacheConfig(cacheNames = "diet.findById")
public class DietServiceImpl implements DietService {

    private final DietRepository repository;

    private final DietMapper mapper;

    @Autowired
    public DietServiceImpl(DietRepository dietRepository, DietMapper dietMapper) {
        this.repository = dietRepository;
        this.mapper = dietMapper;
    }

    @Cacheable(key = "#result.id")
    @Transactional
    @Override
    public DietDto create(final DietDto createDietDto) {
        final Diet diet = repository.save(mapper.convertToEntity(createDietDto));

        return mapper.convertToDto(diet);
    }

    @Cacheable
    @Transactional(readOnly = true)
    @Override
    public DietDto findById(final Long dietId) {
        final Diet diet = repository.findByIdOrThrow(dietId);

        return mapper.convertToDto(diet);
    }

    @Transactional(readOnly = true)
    @Override
    public List<DietDto> findAll() {
        final List<Diet> diets;

        if (Role.ADMIN.existsIn(AuthenticationFacade.getRoles())) {
            diets = repository.findAll();
        } else {
            diets = repository.findByActive(true);
        }

        return diets.stream()
                    .map(mapper::convertToDto)
                    .collect(Collectors.toList());
    }

    @CacheEvict
    @Transactional
    @Override
    public void activateDiet(final Long id) {
        final Diet diet = repository.findByIdOrThrow(id);

        if (!diet.isActive()) {
            diet.setActive(true);
        }
    }

    @CacheEvict
    @Transactional
    @Override
    public void deactivateDiet(final Long id) {
        final Diet diet = repository.findByIdOrThrow(id);

        if (diet.isActive()) {
            diet.setActive(false);
        }
    }
}
