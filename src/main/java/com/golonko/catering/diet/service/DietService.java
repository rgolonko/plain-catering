package com.golonko.catering.diet.service;

import java.util.List;

import com.golonko.catering.diet.dto.DietDto;

public interface DietService {

    DietDto create(DietDto createDietDto);

    DietDto findById(Long id);

    List<DietDto> findAll();

    void activateDiet(Long id);

    void deactivateDiet(Long id);
}
