package com.golonko.catering.diet.controller;

import java.util.List;

import javax.validation.Valid;

import com.golonko.catering.diet.dto.DietDto;
import com.golonko.catering.diet.service.DietService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
public class DietController {

    private final DietService service;

    @Autowired
    public DietController(DietService dietService) {
        this.service = dietService;
    }

    @GetMapping(value = "/diets/{id}")
    public DietDto getDiet(@PathVariable("id") final Long dietId) {
        return service.findById(dietId);
    }

    @GetMapping(value = "/diets")
    public List<DietDto> getDiets() {
        return service.findAll();
    }

    @PostMapping(value = "/diets")
    public DietDto createDiet(@RequestBody @Valid final DietDto createDietDto) {
        return service.create(createDietDto);
    }

    @PutMapping(value = "/diets/{id}/activate")
    public void activateDiet(@PathVariable("id") final Long dietId) {
        service.activateDiet(dietId);
    }

    @PutMapping(value = "/diets/{id}/deactivate")
    public void deactivateDiet(@PathVariable("id") final Long dietId) {
        service.deactivateDiet(dietId);
    }
}
