package com.golonko.catering.diet.entity;

import com.golonko.catering.base.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Diet extends BaseEntity {

    @NotNull
    @Size(min = 3, max = 15)
    private String name;

    @NotNull
    private boolean active;

    private String description;

    @NotNull
    private Integer kcal;

    @NotNull
    private Float price;

    public Diet() {
        // no-arg constructor required by Hibernate
    }

    public Diet(String name, boolean active, String description, Integer kcal, Float price) {
        this.name = name;
        this.active = active;
        this.description = description;
        this.kcal = kcal;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getKcal() {
        return kcal;
    }

    public void setKcal(Integer kcal) {
        this.kcal = kcal;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
