package com.golonko.catering.diet.repository;

import com.golonko.catering.diet.entity.Diet;
import com.golonko.catering.exception.ResourceNotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DietRepositoryCustomImpl implements DietRepositoryCustom {

    private DietRepository dietRepository;

    public DietRepositoryCustomImpl(@Lazy DietRepository dietRepository) {
        this.dietRepository = dietRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Diet findByIdOrThrow(final Long id) {
        return dietRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Diet with id %s not found", id)
                ));
    }
}
