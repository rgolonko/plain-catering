package com.golonko.catering.diet.repository;

import com.golonko.catering.diet.entity.Diet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DietRepository extends JpaRepository<Diet, Long>, DietRepositoryCustom {

    List<Diet> findByActive(boolean active);

}
