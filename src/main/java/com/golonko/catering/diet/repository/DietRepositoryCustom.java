package com.golonko.catering.diet.repository;

import com.golonko.catering.diet.entity.Diet;

public interface DietRepositoryCustom {

    Diet findByIdOrThrow(Long id);

}
