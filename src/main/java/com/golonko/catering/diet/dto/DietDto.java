package com.golonko.catering.diet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class {@link DietDto} is used for deserialization and serialization.
 * {@link #id} take a part only in serialization/response.
 * Rest fields will be deserialized and serialized
 */
public class DietDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotNull
    @Size(min = 3, max = 15)
    private String name;

    @NotNull
    private boolean active;

    private String description;

    @NotNull
    @Range(min = 1000, max = 3000)
    private Integer kcal;

    @NotNull
    @Range(min = 35, max = 80)
    private Float price;

    public DietDto() {
        // no-arg constructor required by Jackson
    }

    public DietDto(Long id, String name, boolean active, String description, Integer kcal, Float price) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.description = description;
        this.kcal = kcal;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getKcal() {
        return kcal;
    }

    public void setKcal(Integer kcal) {
        this.kcal = kcal;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

}
