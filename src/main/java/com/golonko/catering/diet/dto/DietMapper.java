package com.golonko.catering.diet.dto;

import com.golonko.catering.base.Mapper;
import com.golonko.catering.diet.entity.Diet;
import org.springframework.stereotype.Component;

@Component
public final class DietMapper implements Mapper<Diet, DietDto> {

    @Override
    public Diet convertToEntity(final DietDto dto) {
        return new Diet(
                dto.getName(),
                dto.isActive(),
                dto.getDescription(),
                dto.getKcal(),
                dto.getPrice()
        );
    }

    @Override
    public DietDto convertToDto(final Diet entity) {
        return new DietDto(
                entity.getId(),
                entity.getName(),
                entity.isActive(),
                entity.getDescription(),
                entity.getKcal(),
                entity.getPrice()
        );
    }
}
