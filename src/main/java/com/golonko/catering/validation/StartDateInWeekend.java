package com.golonko.catering.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidStartDateInWeekend.class)
@Documented
public @interface StartDateInWeekend {

    String message() default "Diet can't start in weekend";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
