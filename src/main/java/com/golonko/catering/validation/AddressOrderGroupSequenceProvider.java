package com.golonko.catering.validation;

import java.util.ArrayList;
import java.util.List;

import com.golonko.catering.base.AddressDto;
import com.golonko.catering.base.AddressOrderDto;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

public class AddressOrderGroupSequenceProvider implements DefaultGroupSequenceProvider<AddressOrderDto> {

    @Override
    public List<Class<?>> getValidationGroups(AddressOrderDto addressOrderDto) {
        List<Class<?>> defaultGroupSequence = new ArrayList<>();
        defaultGroupSequence.add(AddressOrderDto.class);
        if (addressOrderDto != null && !addressOrderDto.isTheSameAsUsersAddress()) {
            defaultGroupSequence.add(AddressDto.AddressChecks.class);
        }
        return defaultGroupSequence;
    }
}
