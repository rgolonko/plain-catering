package com.golonko.catering.validation;

import java.time.DayOfWeek;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.golonko.catering.order.dto.CreateOrderDto;

public class ValidStartDateInWeekend implements ConstraintValidator<StartDateInWeekend, CreateOrderDto> {

    @Override
    public void initialize(StartDateInWeekend constraintAnnotation) {
    }

    @Override
    public boolean isValid(CreateOrderDto createOrderDto, ConstraintValidatorContext context) {
        if (!createOrderDto.isWeekendIncluded()) {
            if ((createOrderDto.getStartDate().getDayOfWeek() == DayOfWeek.SATURDAY) ||
                    (createOrderDto.getStartDate().getDayOfWeek() == DayOfWeek.SUNDAY)) {
                return false;
            }
        }
        return true;
    }

}
