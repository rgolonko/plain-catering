package com.golonko.catering.exception;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import static com.golonko.catering.config.security.JwtConstants.EXC_REQUEST_ATTRIBUTE;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        String msg = (String) request.getAttribute(EXC_REQUEST_ATTRIBUTE);
        if (msg != null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, msg);
        } else {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unauthorized");
        }
    }
}