package com.golonko.catering.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ErrorHandlingControllerAdvice /*extends ResponseEntityExceptionHandler*/ {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, WebRequest webRequest,
                                                        HttpServletRequest httpServletRequest) {
        Integer status = HttpStatus.BAD_REQUEST.value();
        String[] message = e.getBindingResult().getAllErrors().stream().map(
                objectError -> {
                    String prefix;
                    if (objectError instanceof FieldError) {
                        prefix = ((FieldError) objectError).getField();
                    } else {
                        prefix = objectError.getObjectName();
                    }
                    return String.format("%s: %s", prefix, objectError.getDefaultMessage());
                }).toArray(String[]::new);
        String path = httpServletRequest.getRequestURI();
        ValidationError validationError = new ValidationError(status, message, path);

        return new ResponseEntity<>(validationError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException e, WebRequest webRequest,
                                                              HttpServletRequest httpServletRequest) {
        Integer status = HttpStatus.BAD_REQUEST.value();
        String[] message = e.getConstraintViolations().stream()
                .map(constraintViolation -> {
                    String prefix;
                    if (!constraintViolation.getPropertyPath().toString().equals("")) {
                        prefix = constraintViolation.getPropertyPath().toString();
                    } else {
                        prefix = constraintViolation.getRootBean().getClass().getSimpleName();
                    }
                    return String.format("%s: %s", prefix, constraintViolation.getMessage());
                })
                .toArray(String[]::new);
        String path = httpServletRequest.getRequestURI();
        ValidationError validationError = new ValidationError(status, message, path);

        return new ResponseEntity<>(validationError, HttpStatus.BAD_REQUEST);
    }
}
