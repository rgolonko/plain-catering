package com.golonko.catering.exception;

import org.springframework.http.HttpStatus;

import java.util.Date;

public class ValidationError {

    private final Date timestamp;
    private final Integer status;
    private final String error;
    private final String[] message;
    private final String path;

    ValidationError(Integer status, String[] message, String path) {
        this.timestamp = new Date();
        this.status = status;
        this.error = HttpStatus.valueOf(status).getReasonPhrase();
        this.message = message;
        this.path = path;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String[] getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }
}
