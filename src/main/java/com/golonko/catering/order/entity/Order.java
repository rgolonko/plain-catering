package com.golonko.catering.order.entity;

import com.golonko.catering.base.entity.Address;
import com.golonko.catering.base.entity.BaseEntity;
import com.golonko.catering.user.entity.User;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "\"order\"")
public class Order extends BaseEntity {

    public final static String DEFAULT_FIELD_TO_SORT = "startDate";
    public static final String FIELD_USER = "user";
    private static final String FK_TO_USER = "fk_order_user";

    @NotNull
    private Integer orderDays;

    @NotEmpty
    @ElementCollection
    @BatchSize(size = 10)
    @CollectionTable(name = "order_date",
            joinColumns = @JoinColumn(name = "id"),
            foreignKey = @ForeignKey(name = "order_date_order_id"))
    @Column(name = "date", nullable = false)
    private Set<@NotNull LocalDate> orderDate;

    @NotNull
    private LocalDate startDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DeliveryTime deliveryTime;

    private String comment;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = Address.FIELD_CITY, column = @Column(name = "city_delivery")),
            @AttributeOverride(name = Address.FIELD_STREET, column = @Column(name = "street_delivery")),
            @AttributeOverride(name = Address.FIELD_HOUSE_NUMBER, column = @Column(name = "house_delivery")),
            @AttributeOverride(name = Address.FIELD_FLAT_NUMBER, column = @Column(name = "flat_delivery")),
    })
    @Valid
    @NotNull
    private Address address;

    @NotNull
    private Float priceTotal;

    @NotNull
    @Column(length = 15)
    private String phoneNumber;

    @NotNull
    @ColumnDefault("false")
    private boolean weekendIncluded;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus = PaymentStatus.PENDING;

    @OneToMany(
            mappedBy = OrderDiet.FIELD_ORDER,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<OrderDiet> orderDiets;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = FK_TO_USER))
    private User user;

    public Order() {
        // no-arg constructor required by Hibernate
    }

    public Integer getOrderDays() {
        return orderDays;
    }

    public void setOrderDays(Integer orderDays) {
        this.orderDays = orderDays;
    }

    public Set<LocalDate> getOrderDate() {
        return orderDate == null ? Collections.emptySet() : orderDate;
    }

    public void setOrderDate(Set<LocalDate> orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public DeliveryTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(DeliveryTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Float getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(Float priceTotal) {
        this.priceTotal = priceTotal;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isWeekendIncluded() {
        return weekendIncluded;
    }

    public void setWeekendIncluded(boolean weekendIncluded) {
        this.weekendIncluded = weekendIncluded;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public List<OrderDiet> getOrderDiets() {
        return orderDiets == null ? Collections.EMPTY_LIST : orderDiets;
    }

    public void setOrderDiets(List<OrderDiet> orderDiets) {
        this.orderDiets = orderDiets;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
