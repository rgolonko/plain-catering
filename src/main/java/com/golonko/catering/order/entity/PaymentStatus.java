package com.golonko.catering.order.entity;

public enum PaymentStatus {

    NOT_PAID,
    PENDING,
    PAID,
}
