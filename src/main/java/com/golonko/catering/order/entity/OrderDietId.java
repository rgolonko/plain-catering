package com.golonko.catering.order.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OrderDietId implements Serializable {

    static final String ORDER_ID_FIELD = "orderId";
    static final String DIET_ID_FIELD = "dietId";

    private Long orderId;

    private Long dietId;

    private OrderDietId() {
    }

    public OrderDietId(Long orderId, Long dietId) {
        this.orderId = orderId;
        this.dietId = dietId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getDietId() {
        return dietId;
    }

    public void setDietId(Long dietId) {
        this.dietId = dietId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderDietId that = (OrderDietId) o;

        return Objects.equals(orderId, that.orderId) &&
                Objects.equals(dietId, that.dietId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, dietId);
    }
}
