package com.golonko.catering.order.entity;

import com.golonko.catering.diet.entity.Diet;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class OrderDiet {

    public static final String FIELD_ORDER = "order";
    public static final String FIELD_DIET = "diet";
    private static final String FK_TO_ORDER = "fk_order_diet_order";
    private static final String FK_TO_DIET = "fk_order_diet_diet";

    @EmbeddedId
    private OrderDietId orderDietId;

    @NotNull
    @Min(value = 1)
    private Integer count;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(OrderDietId.ORDER_ID_FIELD)
    @JoinColumn(foreignKey = @ForeignKey(name = FK_TO_ORDER))
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(OrderDietId.DIET_ID_FIELD)
    @JoinColumn(foreignKey = @ForeignKey(name = FK_TO_DIET))
    private Diet diet;

    private OrderDiet() {
        // no-arg constructor required by Hibernate
    }

    public OrderDiet(Order order, Diet diet, Integer count) {
        this.order = order;
        this.diet = diet;
        this.count = count;
        // Hibernate requires creating instance to set primary key from related entities. You can create instance in
        // either constructor or field
        this.orderDietId = new OrderDietId(order.getId(), diet.getId());
    }

    public OrderDietId getOrderDietId() {
        return orderDietId;
    }

    public void setOrderDietId(OrderDietId orderDietId) {
        this.orderDietId = orderDietId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Diet getDiet() {
        return diet;
    }

    public void setDiet(Diet diet) {
        this.diet = diet;
    }
}
