package com.golonko.catering.order.entity;

import java.time.LocalTime;

public enum DeliveryTime {

    MORNING (LocalTime.of(4, 0), LocalTime.of(7, 0)),
    AFTERNOON (LocalTime.of(18, 0), LocalTime.of(0, 0)),
    NIGHT (LocalTime.of(0, 0), LocalTime.of(4, 0));

    private LocalTime from;
    private LocalTime to;

    DeliveryTime(LocalTime from, LocalTime to) {
        this.from = from;
        this.to = to;
    }

    public LocalTime getFrom() {
        return from;
    }

    public LocalTime getTo() {
        return to;
    }
}
