package com.golonko.catering.order.controller;

import java.util.List;

import javax.validation.Valid;

import com.golonko.catering.order.dto.CreateOrderDto;
import com.golonko.catering.order.dto.OrderDietDto;
import com.golonko.catering.order.dto.OrderDto;
import com.golonko.catering.order.entity.Order;
import com.golonko.catering.order.service.OrderService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    private final OrderService service;

    @Autowired
    public OrderController(OrderService orderService) {
        this.service = orderService;
    }

    @PostMapping(value = "/orders")
    public void createOrder(@RequestBody @Valid CreateOrderDto createOrderDto) {
        service.create(createOrderDto);
    }

    @GetMapping(value = "/orders")
    public Page<OrderDto> getOrder(
        @QuerydslPredicate(root = Order.class) Predicate predicate,
        @PageableDefault(sort = Order.DEFAULT_FIELD_TO_SORT, direction = Sort.Direction.DESC) Pageable pageable,
        @RequestParam MultiValueMap<String, String> parameters
    ) {
        return service.findAll(predicate, pageable);
    }

    @GetMapping(value = "/orders/{id}/orderDiets")
    public List<OrderDietDto> getOrderDiet(@PathVariable Long orderId) {
        return service.findAllByOrderId(orderId);
    }
}
