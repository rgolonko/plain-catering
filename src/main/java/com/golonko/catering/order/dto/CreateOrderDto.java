package com.golonko.catering.order.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.golonko.catering.base.AddressOrderDto;
import com.golonko.catering.order.entity.DeliveryTime;
import com.golonko.catering.validation.StartDateInWeekend;

@StartDateInWeekend
public class CreateOrderDto {

    @NotNull
    private Integer orderDays;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private DeliveryTime deliveryTime;

    private String comment;

    @Valid
    @NotNull
    @JsonProperty(value = "addressOrder")
    private AddressOrderDto addressOrderDto;

    @NotNull
    @Pattern(regexp = "^\\d{3}[-\\s]\\d{3}[-\\s]\\d{3}|" +
            "\\d{2}[-\\s]\\d{3}[-\\s]\\d{2}[-\\s]\\d{2}",
            message = "Invalid number")
    private String phoneNumber;

    @NotNull
    private boolean weekendIncluded;

    @Valid
    @NotNull
    private List<OrderDietDto> orderDiets;

    public CreateOrderDto() {
        // no-arq constructor required by Jackson
    }

    public Integer getOrderDays() {
        return orderDays;
    }

    public void setOrderDays(Integer orderDays) {
        this.orderDays = orderDays;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public DeliveryTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(DeliveryTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public AddressOrderDto getAddressOrderDto() {
        return addressOrderDto;
    }

    public void setAddressOrderDto(AddressOrderDto addressOrderDto) {
        this.addressOrderDto = addressOrderDto;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isWeekendIncluded() {
        return weekendIncluded;
    }

    public void setWeekendIncluded(boolean weekendIncluded) {
        this.weekendIncluded = weekendIncluded;
    }

    public List<OrderDietDto> getOrderDiets() {
        return orderDiets;
    }

    public void setOrderDiets(List<OrderDietDto> orderDiets) {
        this.orderDiets = orderDiets;
    }
}
