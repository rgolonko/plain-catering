package com.golonko.catering.order.dto;

import com.golonko.catering.base.AddressDto;
import com.golonko.catering.order.entity.DeliveryTime;
import com.golonko.catering.order.entity.PaymentStatus;

import java.time.LocalDate;
import java.util.Set;

public class OrderDto {

    private Long id;
    private Integer orderDays;
    private Set<LocalDate> orderDate;
    private LocalDate startDate;
    private DeliveryTime deliveryTime;
    private String comment;
    private AddressDto addressDto;
    private Float priceTotal;
    private String phoneNumber;
    private boolean weekendIncluded;
    private PaymentStatus paymentStatus;

    public OrderDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrderDays() {
        return orderDays;
    }

    public void setOrderDays(Integer orderDays) {
        this.orderDays = orderDays;
    }

    public Set<LocalDate> getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Set<LocalDate> orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public DeliveryTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(DeliveryTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public AddressDto getAddressDto() {
        return addressDto;
    }

    public void setAddressDto(AddressDto addressDto) {
        this.addressDto = addressDto;
    }

    public Float getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(Float priceTotal) {
        this.priceTotal = priceTotal;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isWeekendIncluded() {
        return weekendIncluded;
    }

    public void setWeekendIncluded(boolean weekendIncluded) {
        this.weekendIncluded = weekendIncluded;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
