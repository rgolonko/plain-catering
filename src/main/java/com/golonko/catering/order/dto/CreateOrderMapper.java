package com.golonko.catering.order.dto;

import com.golonko.catering.base.AddressMapper;
import com.golonko.catering.base.Mapper;
import com.golonko.catering.order.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateOrderMapper implements Mapper<Order, CreateOrderDto> {

    private final AddressMapper addressMapper;

    @Autowired
    public CreateOrderMapper(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    @Override
    public Order convertToEntity(final CreateOrderDto dto) {
        final Order order = new Order();
        order.setOrderDays(dto.getOrderDays());
        order.setStartDate(dto.getStartDate());
        order.setDeliveryTime(dto.getDeliveryTime());
        order.setComment(dto.getComment());
        order.setAddress(addressMapper.convertToEntity(dto.getAddressOrderDto()));
        order.setPhoneNumber(dto.getPhoneNumber());
        order.setWeekendIncluded(dto.isWeekendIncluded());
        return order;
    }

    @Override
    public CreateOrderDto convertToDto(final Order entity) {
        throw new UnsupportedOperationException("Converting from Order entity to CreateOrderDto not supported");
    }
}
