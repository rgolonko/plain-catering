package com.golonko.catering.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.golonko.catering.diet.dto.DietDto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Class {@link OrderDietDto} is used for deserialization and serialization.
 * {@link #dietId} take a part only in deserialization/request.
 * {@link #dietDto} takes a part only in serialization/response.
 * Rest fields will be deserialized and serialized
 */
public class OrderDietDto {

    @NotNull
    @Min(value = 1)
    @Max(5)
    private Integer count;

    @NotNull
    @Min(value = 1)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long dietId;

    @JsonProperty(value = "diet", access = JsonProperty.Access.READ_ONLY)
    private DietDto dietDto;

    public OrderDietDto() {
        // no-arq constructor required by Jackson
    }

    public OrderDietDto(Integer count, DietDto dietDto) {
        this.count = count;
        this.dietDto = dietDto;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getDietId() {
        return dietId;
    }

    public void setDietId(Long dietId) {
        this.dietId = dietId;
    }

    public DietDto getDietDto() {
        return dietDto;
    }

    public void setDietDto(DietDto dietDto) {
        this.dietDto = dietDto;
    }
}
