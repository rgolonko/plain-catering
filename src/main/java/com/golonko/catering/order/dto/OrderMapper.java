package com.golonko.catering.order.dto;

import com.golonko.catering.base.AddressMapper;
import com.golonko.catering.base.Mapper;
import com.golonko.catering.order.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper implements Mapper<Order, OrderDto> {

    private final AddressMapper addressMapper;

    @Autowired
    public OrderMapper(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    @Override
    public Order convertToEntity(final OrderDto dto) {
        throw new UnsupportedOperationException("Converting from OrderDto to Order entity not supported");
    }

    @Override
    public OrderDto convertToDto(final Order entity) {
        final OrderDto orderDto = new OrderDto();
        orderDto.setId(entity.getId());
        orderDto.setOrderDays(entity.getOrderDays());
        orderDto.setOrderDate(entity.getOrderDate());
        orderDto.setStartDate(entity.getStartDate());
        orderDto.setDeliveryTime(entity.getDeliveryTime());
        orderDto.setComment(entity.getComment());
        orderDto.setAddressDto(addressMapper.convertToDto(entity.getAddress()));
        orderDto.setPriceTotal(entity.getPriceTotal());
        orderDto.setPhoneNumber(entity.getPhoneNumber());
        orderDto.setWeekendIncluded(entity.isWeekendIncluded());
        orderDto.setPaymentStatus(entity.getPaymentStatus());
        return orderDto;
    }
}
