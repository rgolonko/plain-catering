package com.golonko.catering.order.dto;

import com.golonko.catering.base.Mapper;
import com.golonko.catering.diet.dto.DietMapper;
import com.golonko.catering.order.entity.OrderDiet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderDietMapper implements Mapper<OrderDiet, OrderDietDto> {

    private final DietMapper dietMapper;

    @Autowired
    public OrderDietMapper(DietMapper dietMapper) {
        this.dietMapper = dietMapper;
    }

    @Override
    public OrderDiet convertToEntity(final OrderDietDto dto) {
        throw new UnsupportedOperationException("Converting from OrderDto to Order entity not supported");
    }

    @Override
    public OrderDietDto convertToDto(final OrderDiet entity) {
        return new OrderDietDto(
            entity.getCount(),
            dietMapper.convertToDto(entity.getDiet())
        );
    }
}
