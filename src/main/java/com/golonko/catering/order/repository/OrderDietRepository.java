package com.golonko.catering.order.repository;

import java.util.List;

import com.golonko.catering.order.entity.OrderDiet;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDietRepository extends JpaRepository<OrderDiet, Long> {

    @EntityGraph(attributePaths = OrderDiet.FIELD_DIET)
    List<OrderDiet> findAllWithDietByOrderId(Long orderId);
}
