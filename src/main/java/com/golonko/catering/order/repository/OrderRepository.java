package com.golonko.catering.order.repository;

import com.golonko.catering.order.entity.Order;
import com.golonko.catering.order.entity.QOrder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, QuerydslPredicateExecutor<Order>,
                                         QuerydslBinderCustomizer<QOrder> {

    @EntityGraph(attributePaths = "orderDate")
    Page<Order> findAll(Predicate predicate, Pageable pageable);

    @Override
    default void customize(QuerydslBindings bindings, QOrder root) {
        bindings.bind(String.class).first((StringPath path, String value) -> path.equalsIgnoreCase(value));
    }
}
