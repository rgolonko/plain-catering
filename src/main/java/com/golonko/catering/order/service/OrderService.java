package com.golonko.catering.order.service;

import java.util.List;

import com.golonko.catering.order.dto.CreateOrderDto;
import com.golonko.catering.order.dto.OrderDietDto;
import com.golonko.catering.order.dto.OrderDto;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService {

    void create(CreateOrderDto createOrderDto);

    Page<OrderDto> findAll(Predicate predicate, Pageable pageable);

    List<OrderDietDto> findAllByOrderId(Long orderId);
}
