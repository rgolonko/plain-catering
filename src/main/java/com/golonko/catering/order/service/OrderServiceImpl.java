package com.golonko.catering.order.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.golonko.catering.config.security.AuthenticationFacade;
import com.golonko.catering.diet.repository.DietRepository;
import com.golonko.catering.order.dto.CreateOrderDto;
import com.golonko.catering.order.dto.CreateOrderMapper;
import com.golonko.catering.order.dto.OrderDietDto;
import com.golonko.catering.order.dto.OrderDietMapper;
import com.golonko.catering.order.dto.OrderDto;
import com.golonko.catering.order.dto.OrderMapper;
import com.golonko.catering.order.entity.Order;
import com.golonko.catering.order.entity.OrderDiet;
import com.golonko.catering.order.entity.QOrder;
import com.golonko.catering.order.repository.OrderDietRepository;
import com.golonko.catering.order.repository.OrderRepository;
import com.golonko.catering.user.entity.Role;
import com.golonko.catering.user.entity.User;
import com.golonko.catering.user.repository.UserRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    private final UserRepository userRepository;

    private final DietRepository dietRepository;

    private final CreateOrderMapper createOrderMapper;

    private final OrderMapper orderMapper;

    private final OrderDietMapper orderDietMapper;

    private final OrderDietRepository orderDietRepository;

    @Autowired
    public OrderServiceImpl(
        OrderRepository orderRepository, UserRepository userRepository,
        DietRepository dietRepository, CreateOrderMapper createOrderMapper,
        OrderMapper orderMapper, OrderDietMapper orderDietMapper, OrderDietRepository orderDietRepository
    ) {
        this.repository = orderRepository;
        this.userRepository = userRepository;
        this.dietRepository = dietRepository;
        this.createOrderMapper = createOrderMapper;
        this.orderMapper = orderMapper;
        this.orderDietMapper = orderDietMapper;
        this.orderDietRepository = orderDietRepository;
    }

    @Transactional
    @Override
    public void create(final CreateOrderDto createOrderDto) {
        final User user = userRepository.findByIdOrThrow(AuthenticationFacade.getIdLoggedInUser());
        final Order order = createOrderMapper.convertToEntity(createOrderDto);

        if (createOrderDto.getAddressOrderDto().isTheSameAsUsersAddress()) {
            order.setAddress(user.getAddress());
        }
        order.setOrderDiets(createOrderDto.getOrderDiets()
                                          .stream()
                                          .map(orderDietDto -> new OrderDiet(
                                              order,
                                              dietRepository.findByIdOrThrow(orderDietDto.getDietId()),
                                              orderDietDto.getCount()
                                          ))
                                          .collect(Collectors.toList()));
        countTotalPrice(order);
        fillOrderDays(order);

        order.setUser(user);
        repository.save(order);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<OrderDto> findAll(final Predicate predicate, final Pageable pageable) {
        final Page<Order> orders;

        if (Role.ADMIN.existsIn(AuthenticationFacade.getRoles())) {
            orders = repository.findAll(predicate, pageable);
        } else {
            BooleanBuilder predicateForCustomer = new BooleanBuilder(QOrder.order.user.id.eq(AuthenticationFacade
                                                                                                 .getIdLoggedInUser())).and(predicate);
            orders = repository.findAll(predicateForCustomer, PageRequest.of(pageable.getPageNumber(), pageable
                .getPageSize(), Sort.by(Sort.Direction.DESC, Order.DEFAULT_FIELD_TO_SORT)));
        }

        return orders.map(orderMapper::convertToDto);
    }

    @Transactional(readOnly = true)
    @Override
    public List<OrderDietDto> findAllByOrderId(Long orderId) {
        final List<OrderDiet> orderDietsInDb = orderDietRepository.findAllWithDietByOrderId(orderId);

        return orderDietsInDb.stream()
                             .map(orderDietMapper::convertToDto)
                             .collect(Collectors.toList());
    }

    private void countTotalPrice(final Order order) {
        order.setPriceTotal((float) order.getOrderDiets()
                                         .stream()
                                         .mapToDouble((orderDiet) -> orderDiet.getDiet().getPrice() * orderDiet.getCount())
                                         .sum());
    }

    private void fillOrderDays(final Order order) {
        final Set<LocalDate> orderDays = new TreeSet<>();

        LocalDate tempDate = order.getStartDate();
        int addedDays = 1;
        if (!order.isWeekendIncluded()) {
            while (addedDays <= order.getOrderDays()) {
                if (!(tempDate.getDayOfWeek() == DayOfWeek.SATURDAY || tempDate.getDayOfWeek() == DayOfWeek.SUNDAY)) {
                    addedDays++;
                    orderDays.add(tempDate);
                }
                tempDate = tempDate.plusDays(1);
            }
        } else {
            for (int i = addedDays; i <= order.getOrderDays(); i++) {
                orderDays.add(tempDate);
                tempDate = tempDate.plusDays(1);
            }
        }
        order.setOrderDate(orderDays);
    }
}
