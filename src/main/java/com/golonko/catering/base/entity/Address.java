package com.golonko.catering.base.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class Address {

    public static final String FIELD_CITY = "city";
    public static final String FIELD_STREET = "street";
    public static final String FIELD_HOUSE_NUMBER = "houseNumber";
    public static final String FIELD_FLAT_NUMBER = "flatNumber";

    @NotNull
    private String city;

    @NotNull
    private String street;

    @NotNull
    private Integer houseNumber;

    private Integer flatNumber;

    public Address() {
        // no-arg constructor required by Hibernate
    }

    public Address(String city, String street, Integer houseNumber, Integer flatNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(Integer flatNumber) {
        this.flatNumber = flatNumber;
    }
}
