package com.golonko.catering.base;

import com.golonko.catering.validation.AddressOrderGroupSequenceProvider;
import org.hibernate.validator.group.GroupSequenceProvider;

/**
 * Class extends {@link AddressDto} which has field validation.
 * Nevertheless, when {@link AddressOrderDto#theSameAsUsersAddress} is true it allows to have nullable fields.
 * The validation works only if {@link AddressOrderDto#theSameAsUsersAddress} is false.
 * {@link AddressOrderGroupSequenceProvider} is responsible for that settings.
 */
@GroupSequenceProvider(AddressOrderGroupSequenceProvider.class)
public class AddressOrderDto extends AddressDto {

    private boolean theSameAsUsersAddress;

    public boolean isTheSameAsUsersAddress() {
        return theSameAsUsersAddress;
    }

    public void setTheSameAsUsersAddress(boolean theSameAsUsersAddress) {
        this.theSameAsUsersAddress = theSameAsUsersAddress;
    }
}
