package com.golonko.catering.base;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.golonko.catering.validation.AddressOrderGroupSequenceProvider;

/**
 * {@link GroupSequence} override default group during validation only for {@link AddressDto}
 * Default group has been overwritten because validation is needed when new user is created. {@link AddressDto} is
 * also used by {@link com.golonko.catering.user.dto.UserDto}
 * @see AddressOrderDto
 * @see AddressOrderGroupSequenceProvider
 */
@GroupSequence(value = {AddressDto.AddressChecks.class, AddressDto.class})
public class AddressDto {

    @NotBlank(groups = AddressChecks.class)
    private String city;

    @NotBlank(groups = AddressChecks.class)
    private String street;

    @NotNull(groups = AddressChecks.class)
    @Positive(groups = AddressChecks.class)
    private Integer houseNumber;

    @Positive(groups = AddressChecks.class)
    private Integer flatNumber;

    public AddressDto() {
        // no-arq constructor required by Jackson
    }

    AddressDto(String city, String street, Integer houseNumber, Integer flatNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(Integer flatNumber) {
        this.flatNumber = flatNumber;
    }

    public interface AddressChecks {
    }
}

