package com.golonko.catering.base;

import com.golonko.catering.base.entity.Address;
import org.springframework.stereotype.Component;

@Component
public final class AddressMapper implements Mapper<Address, AddressDto> {

    @Override
    public Address convertToEntity(final AddressDto dto) {
        return new Address(
                dto.getCity(),
                dto.getStreet(),
                dto.getHouseNumber(),
                dto.getFlatNumber()
        );
    }

    @Override
    public AddressDto convertToDto(final Address entity) {
        return new AddressDto(
                entity.getCity(),
                entity.getStreet(),
                entity.getHouseNumber(),
                entity.getFlatNumber()
        );
    }
}
