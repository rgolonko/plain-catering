package com.golonko.catering.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Cache Configuration
 */
@Configuration
@EnableCaching
public class CacheConfig {
}
