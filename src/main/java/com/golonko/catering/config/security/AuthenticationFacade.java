package com.golonko.catering.config.security;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationFacade {

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private static UserDetailsImpl getUserDetails() {
        return (UserDetailsImpl) getAuthentication().getPrincipal();
    }

    public static Collection<String> getRoles() {
        return getAuthentication().getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
    }

    public static String getUsername() {
        return getAuthentication().getName();
    }

    public static Long getIdLoggedInUser() {
        return getUserDetails().getId();
    }

}
