package com.golonko.catering.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import static com.golonko.catering.config.security.JwtConstants.EXC_REQUEST_ATTRIBUTE;
import static com.golonko.catering.config.security.JwtConstants.JWT_HEADER;
import static com.golonko.catering.config.security.JwtConstants.JWT_TOKEN_PREFIX;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private final UserDetailsService userDetailsService;

    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public JwtRequestFilter(UserDetailsService userDetailsServiceImpl, JwtTokenUtil jwtTokenUtil) {
        this.userDetailsService = userDetailsServiceImpl;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {

        final String authorizationHeader = request.getHeader(JWT_HEADER);
        String username = null;
        String jwt = null;
        boolean validToken = false;
        UserDetails userDetails = null;

        if (authorizationHeader != null && authorizationHeader.startsWith(JWT_TOKEN_PREFIX)) {
            jwt = authorizationHeader.substring(7);
            try {
                username = jwtTokenUtil.extractUsername(jwt);
                userDetails = this.userDetailsService.loadUserByUsername(username);
                validToken = jwtTokenUtil.validateToken(jwt, userDetails);
            } catch (JwtException | UsernameNotFoundException e) {
                request.setAttribute(EXC_REQUEST_ATTRIBUTE, e.getMessage());
            }
        }

        if (username != null && userDetails != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            if (validToken) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request, response);

    }
}
