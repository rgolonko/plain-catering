package com.golonko.catering.config.security;

public class JwtConstants {

    public static final long JWT_TOKEN_VALIDITY_MILISECONDS = 1000 * 60 * 60 * 10;
    public static final String JWT_SIGNING_KEY = "secret";
    public static final String JWT_HEADER = "Authorization";
    public static final String JWT_TOKEN_PREFIX = "Bearer ";
    public static final String EXC_REQUEST_ATTRIBUTE = "jwtException";
}
