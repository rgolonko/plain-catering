package com.golonko.catering.config.security;

public class JwtRequest {

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
