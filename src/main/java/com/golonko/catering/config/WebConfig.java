package com.golonko.catering.config;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.golonko.catering.base.converter.LocalDateCustomConverter;
import com.golonko.catering.base.converter.LocalDateTimeCustomConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WEB Configuration
 */
@EnableWebMvc
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateCustomConverter());
        registry.addConverter(new LocalDateTimeCustomConverter());
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> builder
            .failOnUnknownProperties(false)
            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .modules(new JavaTimeModule())
            .visibility(PropertyAccessor.ALL, Visibility.NONE)
            .visibility(PropertyAccessor.CREATOR, Visibility.PROTECTED_AND_PUBLIC)
            .visibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    // Customize already created bean PageableHandlerMethodArgumentResolver delivered from SpringData
    @Bean
    public PageableHandlerMethodArgumentResolverCustomizer pageableCustomizer() {
        return resolver -> {
            resolver.setOneIndexedParameters(true);
            resolver.setFallbackPageable(PageRequest.of(1, 10));
        };
    }

    /**
     * Workaround for Spring WebMVC ignoring global ObjectMapper
     */
    @Configuration
    public static class WebMvcDefaultObjectMapperConfigurerAdapter implements WebMvcConfigurer {
        private final ObjectMapper objectMapper;

        @Autowired
        public WebMvcDefaultObjectMapperConfigurerAdapter(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
        }

        @Override
        public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.stream()
                      .filter(MappingJackson2HttpMessageConverter.class::isInstance)
                      .forEach(converter -> ((MappingJackson2HttpMessageConverter) converter).setObjectMapper(objectMapper));
        }
    }
}
