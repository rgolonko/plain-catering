package com.golonko.catering.user.entity;

import com.golonko.catering.base.entity.Address;
import com.golonko.catering.base.entity.BaseEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(
                name = "uk_user_email",
                columnNames = "email"
        ),
        @UniqueConstraint(
                name = "uk_user_login",
                columnNames = "login"
        )
})
public class User extends BaseEntity {

    public static final String DEFAULT_FIELD_TO_SORT = "login";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    @NotNull
    private String email;

    @NotNull
    private String login;

    @NotNull
    private String password;

    @Embedded
    @Valid
    @NotNull
    private Address address;

    public User() {
        // no-arg constructor required by Hibernate
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
