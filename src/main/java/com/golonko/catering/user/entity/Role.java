package com.golonko.catering.user.entity;

import java.util.Collection;

public enum Role {

    ADMIN("ROLE_ADMIN"),
    CUSTOMER("ROLE_CUSTOMER"),
    COOKER("ROLE_COOKER");

    private final String fullName;

    Role(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean existsIn(Collection<String> fullNames) {
        return fullNames.contains(fullName);
    }

    public static Role fromFullName(String fullName) {
        for (Role role : values()) {
            if (role.fullName.equals(fullName)) {
                return role;
            }
        }
        return null;
    }
}
