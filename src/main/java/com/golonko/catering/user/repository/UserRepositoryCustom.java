package com.golonko.catering.user.repository;

import com.golonko.catering.user.entity.User;

public interface UserRepositoryCustom {

    User findByIdOrThrow(Long id);
}
