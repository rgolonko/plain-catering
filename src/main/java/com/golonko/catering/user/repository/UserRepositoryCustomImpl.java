package com.golonko.catering.user.repository;

import com.golonko.catering.exception.ResourceNotFoundException;
import com.golonko.catering.user.entity.User;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {

    private UserRepository userRepository;

    public UserRepositoryCustomImpl(@Lazy UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public User findByIdOrThrow(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("User with id %s not found", id))
                );
    }
}
