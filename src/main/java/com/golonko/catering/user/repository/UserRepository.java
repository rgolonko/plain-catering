package com.golonko.catering.user.repository;

import com.golonko.catering.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

    Optional<User> findByLogin(String login);

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    @Query("SELECT u FROM User u WHERE (:login is null or u.login like concat('%', :login ,'%') ) " +
            "and (:fromCreatedOn is null or u.createdOn > :fromCreatedOn)" +
            "and (:addressCity is null or UPPER(u.address.city) = UPPER(:addressCity)) " +
            "and (:addressStreet is null or UPPER(u.address.street) = UPPER(:addressStreet))")
    Page<User> findUserByLoginAndAddressStreetAndCityAndCreatedOn(
            @Param("login") String login,
            @Param("fromCreatedOn") LocalDateTime fromCreatedOn,
            @Param("addressCity") String addressCity,
            @Param("addressStreet") String addressStreet,
            Pageable pageable);
}
