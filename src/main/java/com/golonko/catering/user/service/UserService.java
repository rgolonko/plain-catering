package com.golonko.catering.user.service;

import com.golonko.catering.user.dto.UserDto;
import com.golonko.catering.user.dto.UserFilter;
import com.golonko.catering.user.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    UserDto create(UserDto userRequestDto, Role role);

    UserDto findById(Long id);

    Page<UserDto> findAll(UserFilter userFilter, Pageable pageable);
}
