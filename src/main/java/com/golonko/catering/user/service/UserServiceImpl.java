package com.golonko.catering.user.service;

import com.golonko.catering.exception.UserExistsException;
import com.golonko.catering.user.dto.UserDto;
import com.golonko.catering.user.dto.UserFilter;
import com.golonko.catering.user.dto.UserMapper;
import com.golonko.catering.user.entity.Role;
import com.golonko.catering.user.entity.User;
import com.golonko.catering.user.repository.UserRepository;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CacheConfig(cacheNames = "user.findById")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Cacheable(key = "#result.id")
    @Transactional
    @Override
    public UserDto create(final UserDto userRequestDto, final Role role) {
        this.isUserWithLoginExists(userRequestDto.getLogin());
        this.isUserWithEmailExists(userRequestDto.getEmail());
        final User user = userMapper.convertToEntity(userRequestDto);
        user.setRole(role);
        userRepository.save(user);
        return userMapper.convertToDto(user);
    }

    @Cacheable
    @Transactional(readOnly = true)
    @Override public UserDto findById(Long id) {
        final User user = userRepository.findByIdOrThrow(id);

        return userMapper.convertToDto(user);
    }

    @Transactional
    @Override
    public Page<UserDto> findAll(final UserFilter userFilter, final Pageable pageable) {
        final Page<User> userInDb = userRepository.findUserByLoginAndAddressStreetAndCityAndCreatedOn(
            userFilter.getLogin(), userFilter.getFromCreatedOn(), userFilter.getAddressCity(),
            userFilter.getAddressStreet(), pageable
        );
        return userInDb.map(user -> userMapper.convertToDto(user));
    }

    private void isUserWithLoginExists(final String login) {
        if (userRepository.existsByLogin(login)) {
            throw new UserExistsException(
                String.format("User with login: %s exists.", login));
        }
    }

    private void isUserWithEmailExists(final String email) {
        if (userRepository.existsByEmail(email)) {
            throw new UserExistsException(
                String.format("User with email: %s exists.", email));
        }
    }
}
