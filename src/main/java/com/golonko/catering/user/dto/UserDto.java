package com.golonko.catering.user.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.golonko.catering.base.AddressDto;
import com.golonko.catering.validation.ValidPassword;

/**
 * Class {@link UserDto} is used for deserialization and serialization.
 * {@link #password} take a part only in deserialization/request.
 * {@link #id} take a part only in serialization/response.
 * Rest fields will be deserialized and serialized
 */
public final class UserDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotNull
    @Email
    private String email;

    @NotBlank
    private String login;

    @ValidPassword
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Valid
    @NotNull
    @JsonProperty(value = "address")
    private AddressDto addressDto;

    public UserDto() {
        // no-arq constructor required by Jackson
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AddressDto getAddressDto() {
        return addressDto;
    }

    public void setAddressDto(AddressDto addressDto) {
        this.addressDto = addressDto;
    }
}
