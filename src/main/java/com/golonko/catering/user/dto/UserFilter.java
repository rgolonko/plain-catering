package com.golonko.catering.user.dto;

import java.time.LocalDateTime;

public class UserFilter {

    private LocalDateTime fromCreatedOn;
    private String login;
    private String addressCity;
    private String addressStreet;

    public UserFilter() {
    }

    public LocalDateTime getFromCreatedOn() {
        return fromCreatedOn;
    }

    public void setFromCreatedOn(LocalDateTime fromCreatedOn) {
        this.fromCreatedOn = fromCreatedOn;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }
}
