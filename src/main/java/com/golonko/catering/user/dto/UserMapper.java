package com.golonko.catering.user.dto;

import com.golonko.catering.base.AddressMapper;
import com.golonko.catering.base.Mapper;
import com.golonko.catering.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public final class UserMapper implements Mapper<User, UserDto> {

    private PasswordEncoder passwordEncoder;
    private AddressMapper addressMapper;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder, AddressMapper addressMapper) {
        this.passwordEncoder = passwordEncoder;
        this.addressMapper = addressMapper;
    }

    @Override
    public User convertToEntity(final UserDto userDto) {
        final User user = new User();
        user.setEmail(userDto.getEmail());
        user.setLogin(userDto.getLogin());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setAddress(addressMapper.convertToEntity(userDto.getAddressDto()));
        return user;
    }

    @Override
    public UserDto convertToDto(final User user) {
        final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setLogin(user.getLogin());
        userDto.setAddressDto(addressMapper.convertToDto(user.getAddress()));
        return userDto;
    }

}
