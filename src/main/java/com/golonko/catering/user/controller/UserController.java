package com.golonko.catering.user.controller;

import javax.validation.Valid;

import com.golonko.catering.config.security.JwtRequest;
import com.golonko.catering.config.security.JwtResponse;
import com.golonko.catering.config.security.JwtTokenUtil;
import com.golonko.catering.exception.InvalidCredentialsException;
import com.golonko.catering.user.dto.UserDto;
import com.golonko.catering.user.dto.UserFilter;
import com.golonko.catering.user.entity.Role;
import com.golonko.catering.user.entity.User;
import com.golonko.catering.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    private final UserService userService;

    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public UserController(
        AuthenticationManager authenticationManager, UserDetailsService userDetailsServiceImpl,
        UserService userService, JwtTokenUtil jwtTokenUtil
    ) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsServiceImpl;
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping(value = "/register")
    public UserDto createUser(@RequestBody @Valid final UserDto userRequestDto) {
        return userService.create(userRequestDto, Role.CUSTOMER);
    }

    @GetMapping(value = "/users/{id}")
    public UserDto getUser(@PathVariable Long id) {
        return userService.findById(id);
    }

    @GetMapping(value = "/users")
    public Page<UserDto> getUsers(
        UserFilter userFilter,
        @PageableDefault(sort = User.DEFAULT_FIELD_TO_SORT) Pageable pageable
    ) {
        return userService.findAll(userFilter, pageable);
    }

    @PostMapping(value = "/authenticate")
    public JwtResponse createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new InvalidCredentialsException();
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getUsername());
        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return new JwtResponse(jwt);
    }
}
