FROM openjdk:11-jdk-slim-buster AS builder

ENV TERM "dumb"
ENV DEBIAN_FRONTEND "noninteractive"

RUN apt-get update -qq
RUN apt-get upgrade -y -qq > /dev/null

# required due to bug between debian:buster-slim docker image and java
RUN mkdir -p /usr/share/man/man1

RUN apt-get install -y -qq --no-install-recommends maven > /dev/null
RUN apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt/build/backend/
WORKDIR /opt/build/backend/

COPY pom.xml /opt/build/backend/

RUN mvn -q dependency:go-offline

COPY src /opt/build/backend/src/
COPY .git /opt/build/backend/.git/

RUN mvn -q -Dmaven.test.skip=true clean package

#####################################

FROM openjdk:14-slim-buster

ENV TERM "dumb"
ENV DEBIAN_FRONTEND "noninteractive"

RUN groupadd spring -g 999
RUN useradd spring -g spring -u 999

RUN mkdir -p /app/logs
RUN chown spring:spring /app/logs

VOLUME /app/logs

RUN apt-get update -qq
RUN apt-get upgrade -y -qq
RUN apt-get install -y --no-install-recommends default-mysql-client > /dev/null
RUN apt-get clean autoclean
RUN apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY --from=builder /opt/build/backend/target/plain-catering.jar /app/plain-catering.jar

COPY /misc/wait-for-it.sh /app/wait-for-it.sh
RUN chmod +x wait-for-it.sh

USER spring:spring

ENTRYPOINT ["./wait-for-it.sh", "plain-catering-database:3306", "--", "java", "-jar", "plain-catering.jar"]