# Plain Catering App

## General info
Application supporting the catering order process. It allows you to place an order as well as create individual diet packages for ordering. \
This is a REST app designed to develop my coding skills and knowledge of building a database structure. The app will be constantly improved in line with gaining experience by adding new tools and features. This project is build using [technologies](#Technologies) below.

## Screenshots
*Database model created from scratch*

![Database Model](./misc/database-model.jpg)

## Technologies
* Java 11
* Spring Boot
  * Spring MVC
  * Spring Data
  * Spring Security with JSON Web Token
* Hibernate/JPA with validator
* MySQL
* Maven
* Docker
* Ehcache
* Query DSL
* JUnit, Mockito and PowerMock

## Features
* Register and logging in by using JWT Token
* Creating order of chosen diet
* Diet management (create, delete)

To-do list:
* ~~Caching - done~~
* ~~Docker configuration - done~~
* Code refactor
* Logback.xml configuration
* More endpoints to diet and order management (editing)
* Swagger
* Spring Actuator
* Frontend (React)

## Status
**In Progress** - constantly improving 